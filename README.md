**Project moved to GitHub**
https://github.com/lukas-mertens/facilethings-wrapper

# An Unofficial FacileThings Desktop Application

This application wraps FacileThings into an electron app and adds a few small userscripts.

## Latest Builds
 - [Linux](https://gitlab.com/lukas-mertens/facilethings-wrapper/-/jobs/artifacts/master/raw/build/FacileThings-linux-x64.tar.gz?job=build:linux)
 - [Windows](https://gitlab.com/lukas-mertens/facilethings-wrapper/-/jobs/artifacts/master/raw/build/FacileThings-win32-x64.tar.gz?job=build:windows)
 - [MacOS](https://gitlab.com/lukas-mertens/facilethings-wrapper/-/jobs/artifacts/master/raw/build/FacileThings-darwin-x64.tar.gz?job=build:mac)
